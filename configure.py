from configparser import ConfigParser


def read_config_info(filename="config.ini", section="dbms"):
    parser = ConfigParser()
    parser.read(filename)

    config_info = {}

    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            config_info[item[0]] = item[1]
    else:
        raise Exception("{0} not found in the {1} file".format(section, filename))

    return config_info
