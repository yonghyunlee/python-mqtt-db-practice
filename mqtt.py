import json
from configparser import ConfigParser
import paho.mqtt.client as mqtt_client

from configure import read_config_info
from persistent import persistent_data


def on_connect(client, obj, flags, rc):
    print("You have succesfully connected with the server.")


def on_message(client, obj, message):
    data = json.loads(str(message.payload.decode("utf-8")))
    print(data)
    name = data["name"]
    age = data["age"]

    print("name: ", name, "age: ", age)
    persistent_data(name, age)


def on_publish(client, obj, message_id):
    print("mid: ")


def connect():
    broker_info = read_config_info(section="mqtt")

    client = mqtt_client.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.username_pw_set(broker_info["user"], broker_info["password"])

    client.connect(broker_info["host"], int(broker_info["port"]), int(broker_info["keep_alive_interval"]))

    return client


def subscribe():
    broker_info = read_config_info(section="mqtt")

    client = connect()
    client.subscribe(broker_info["topic"], int(broker_info["qos"]))

    client.loop_forever()
