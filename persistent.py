from configparser import ConfigParser

import psycopg2

from configure import read_config_info


def persistent_data(name, age):
    if find_by_name(name):
        update_by_name(name, age)
    else:
        insert(name, age)


def read_once(query, args):
    db_info = read_config_info(section="dbms")

    connection = None
    cursor = None
    try:
        connection = psycopg2.connect(**db_info)

        if connection:
            print("Connection established")
            cursor = connection.cursor()
            cursor.execute(query, args)
            find_value = cursor.fetchone()
            return find_value
        else:
            print("Connection failed.")
    except (Exception, psycopg2.Error) as error:
        print(error)
    finally:
        cursor.close()
        connection.close()
        print("Connection closed")


def read_all(query, args):
    db_info = read_config_info(section="dbms")

    connection = None
    cursor = None
    try:
        connection = psycopg2.connect(**db_info)

        if connection:
            print("Connection established")
            cursor = connection.cursor()
            cursor.execute(query, args)
            find_values = cursor.fetchall()
            return find_values
        else:
            print("Connection failed.")
    except (Exception, psycopg2.Error) as error:
        print(error)
    finally:
        cursor.close()
        connection.close()
        print("Connection closed")

def execute(query, args):
    db_info = read_config_info(section="dbms")

    connection = None
    cursor = None
    try:
        connection = psycopg2.connect(**db_info)

        if connection:
            print("Connection established.")
            cursor = connection.cursor()
            cursor.execute(query, args)
            count = cursor.rowcount
            print(count, " Executed.")
            connection.commit()
        else:
            print("Connection failed.")
    except (Exception, psycopg2.Error) as error:
        print(error)
    finally:
        if connection:
            cursor.close()
            connection.close()
            print("Connection closed")


def insert(name, age):
    query = """ 
        INSERT INTO account(name, age, update_datetime)
        VALUES (%s, %s, CURRENT_TIMESTAMP(6))
    """
    args = (name, age, )

    execute(query, args)


def update_by_name(name, age):
    db_info = read_config_info(section="dbms")

    query = """
        UPDATE account
        SET age = %s, update_datetime = CURRENT_TIMESTAMP (6)
        WHERE name = %s
    """

    args = (age, name, )

    execute(query, args)


def remove_by_name(name):
    db_info = read_config_info(section="dbms")

    query = """
            DELETE FROM account where name = %s
        """

    args = (name,)

    execute(query, args)


def find_by_name(name):
    db_info = read_config_info(section="dbms")
    query = """
        SELECT name FROM account WHERE name = %s
    """

    args = (name,)

    return read_once(query, args)


def read_db_info(filename="config.ini", section="dbms"):
    parser = ConfigParser()
    parser.read(filename)

    db_info = {}

    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            db_info[item[0]] = item[1]
    else:
        raise Exception("{0} not found in the {1} file".format(section, filename))

    return db_info


